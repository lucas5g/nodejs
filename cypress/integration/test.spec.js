/// <reference types="cypress" />
describe('test app', () => {

    it('test api', () => {

        cy.request({
                url: '/test',
                method: 'get'
            })
            .then(res => {
                const { body } = res

                expect(body.user).property('name', 'lucas')
            })
    })

})
