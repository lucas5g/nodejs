import express from 'express'

const app = express()


// app.get('/', ())
app.get('/', (req, res) => {
    res.json({
        msg: 'api test',
        info: 'run test',
        date: 'run'
    })
})

app.get('/test', (req, res) => {
    res.json({
        user: {
            name: 'lucas',
            age: '29',
            test: "qwe"
        }
    })
})
const PORT = 8001
app.listen(PORT, () => console.log(`http://localhost:${PORT}`))
